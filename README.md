# TextminingAPI DB Manager

## Local Installation and Deployment

### Prerequisites

- [Textmining Utility][1]

### Logging

For logging to work, you need to set the following JVM options:

`HOSTNAME` and `LOGPATH`

as their values are used to determine the path of the log files "LOGPATH/logs/textmining_api_db_manager-HOSTNAME.log".


### Starting the Project

- Since this is a Spring Boot project, you can start it by running the main method of the `TextminingApiDBManagerApplication` class.


[1]: https://gitlab.ebi.ac.uk/literature-services/public-projects/textmining-utility