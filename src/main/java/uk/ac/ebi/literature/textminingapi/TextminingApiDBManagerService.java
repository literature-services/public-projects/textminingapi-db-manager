package uk.ac.ebi.literature.textminingapi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import uk.ac.ebi.literature.textminingapi.pojo.AnnotationsData;
import uk.ac.ebi.literature.textminingapi.pojo.Components;
import uk.ac.ebi.literature.textminingapi.pojo.MLTextObject;
import uk.ac.ebi.literature.textminingapi.pojo.Status;
import uk.ac.ebi.literature.textminingapi.service.FileService;
import uk.ac.ebi.literature.textminingapi.service.MLQueueSenderService;
import uk.ac.ebi.literature.textminingapi.service.MongoService;
import uk.ac.ebi.literature.textminingapi.utility.Utility;

@Service
public class TextminingApiDBManagerService {

	private static final Logger log = LoggerFactory.getLogger(TextminingApiDBManagerService.class);

	@Value("${rabbitmq.tmExchange}")
	private String textminingExchange;

	@Value("${rabbitmq.outcomeQueue}")
	private String textminingOutcomeQueue;

	@Value("${rabbitmq.jsonForApiQueue}")
	private String textminingJsonForApiQueue;

	@Value("${mongo.transaction}")
	private boolean transactionMongo;

	private final MLQueueSenderService mlQueueSenderService;

	private final MongoService mongoService;

	private final FileService fileService;

	public TextminingApiDBManagerService(MLQueueSenderService mlQueueSenderService, MongoService mongoService, FileService fileService) {
		this.mlQueueSenderService = mlQueueSenderService;
		this.mongoService = mongoService;
		this.fileService = fileService;
	}

	@RabbitListener(autoStartup = "${rabbitmq.jsonForApiQueue.autoStartUp}", queues = "${rabbitmq.jsonForApiQueue}")
	public void listenForMessage(Message message) throws Exception {
		MLTextObject mlTextObject = Utility.castMessage(message, MLTextObject.class);
		if (mlQueueSenderService.hasExceededRetryCount(message)) {
			log.info("{textminingJsonForApiQueue Message" + mlTextObject.toString() + "} retry count exceeded");

			Utility.markMessageAsFailed(mlTextObject, Components.DB_MANAGER);

			mlQueueSenderService.sendMessageToQueue(textminingOutcomeQueue, mlTextObject, textminingExchange);

			boolean deleted = fileService.delete(mlTextObject.getProcessingFilename());
			log.info("{" + mlTextObject.getFtId() + "," + mlTextObject.getUser() + "} Deleted "
					+ mlTextObject.getProcessingFilename() + "? " + deleted);
		} else {

			log.info(" Storing annotations for file " + mlTextObject.toString());
			byte[] dataApi = fileService.read(mlTextObject.getProcessingFilename());

			AnnotationsData val = Utility.parseJsonObject(dataApi, AnnotationsData.class);
			this.fillIdentifiersData(val, mlTextObject);
			storeAnnotations(val);

			mlTextObject.setStatus(Status.SUCCESS.getLabel());

			boolean sending = mlQueueSenderService.sendMessageToQueue(this.textminingOutcomeQueue, mlTextObject,
					textminingExchange);

			if (sending == false) {
				throw new Exception("Impossible to store Success in Outcome queue  for " + mlTextObject.toString());
			}

			fileService.delete(mlTextObject.getProcessingFilename());
			log.info("{ DB Manager successfully processed for " + mlTextObject.toString());

		}
	}

	private void fillIdentifiersData(AnnotationsData val, MLTextObject mlTextObject) {
		val.setUser(mlTextObject.getUser());
		val.setFtId(mlTextObject.getFtId());
		val.setFilename(mlTextObject.getFilename());
	}

	private void storeAnnotations(AnnotationsData annData) throws Exception {
		if (this.transactionMongo) {
			this.mongoService.storeAnnotations(annData);
		} else {
			this.mongoService.storeAnnotationsNoTransactional(annData);
		}
	}

}
